/*
 * Bee LED driver  
 * Data: 20180518
 */
#include<linux/device.h>

#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/device.h>
#include <linux/gpio.h>
#include "rpdzkj-sysfs.h"
#ifdef CONFIG_OF
#include <linux/of.h>
#include <linux/of_device.h>
#include <linux/of_gpio.h>
#endif
#include <linux/timer.h>

#include <asm/uaccess.h>

#include <linux/of_gpio.h>
#include <linux/fb.h>
#include <linux/backlight.h>
#include <linux/err.h>
#include <linux/pwm.h>
#include <linux/pwm_backlight.h>
#include <linux/slab.h>

struct led_data *bee_led;
static struct timer_list LedTimer;
static int led_recovery_flag  = 0;

static ssize_t recovery_write(struct device* dev, struct device_attribute* attr, const char* buf, size_t count){

	led_recovery_flag = buf[0] - 0x30;
	printk("led_flash_status = %d\n",led_recovery_flag);

	if(led_recovery_flag == 2){
		gpio_direction_output(bee_led->led_gpio.gpio,1);
		bee_led->led_gpio.enable = 1;
	}else if(led_recovery_flag == 1){
		gpio_direction_output(bee_led->led_gpio.gpio,0);
		bee_led->led_gpio.enable = 0;
	}
	return count;
}

static ssize_t recovery_read(struct device* dev, struct device_attribute* attr, char* buf){
	printk("led_flash_status = %d\n",led_recovery_flag);
	return 0;
}

static DEVICE_ATTR(recovery, 0666, recovery_read, recovery_write);

void led_ctrl_func(unsigned long data)
{
	if(led_recovery_flag == 2){
		gpio_direction_output(bee_led->led_gpio.gpio,1);
		bee_led->led_gpio.enable = 1;
        	mod_timer(&LedTimer, jiffies + msecs_to_jiffies(2000));
		return;
	}else if(led_recovery_flag == 1){
		gpio_direction_output(bee_led->led_gpio.gpio,0);
		bee_led->led_gpio.enable = 0;
        	mod_timer(&LedTimer, jiffies + msecs_to_jiffies(2000));
		return;
	}
        bee_led->led_gpio.enable ^= 1;
        gpio_direction_output(bee_led->led_gpio.gpio,bee_led->led_gpio.enable);
        mod_timer(&LedTimer, jiffies + msecs_to_jiffies(2000));

        return;
}

static int bee_led_gpio_probe(struct platform_device *pdev)
{
	
	struct class *bee_led_class;
	int ret;
	int init_flag = 0;

#ifdef CONFIG_OF
	enum of_gpio_flags flags;
	struct device_node *node = pdev->dev.of_node;
#endif

	if(node != NULL){
		if(of_property_read_u32(node, "driver_init", &init_flag)){
			printk("of_property_read_u32 fail !!!\n");
			return -EINVAL;
		}else{
			if(init_flag != 1){
				return 0;
			}
		}
	}else{
		printk("%s(), device node is null !!!\n", __FUNCTION__);
		return -ENOMEM;
	}

	if(!bee_led){
		bee_led = kzalloc(sizeof(struct led_data), GFP_KERNEL);
		if (!bee_led)
			return -ENOMEM;
			memset(bee_led, 0, sizeof(struct led_data));
	}
	if(!node){
		if(!bee_led)
			kfree(bee_led);
		return 0;
	}
#ifdef CONFIG_OF
	bee_led->led_gpio.gpio = of_get_named_gpio_flags(node, "led_gpio", 0, &flags);
	if (gpio_is_valid(bee_led->led_gpio.gpio)){
		bee_led->led_gpio.enable = (flags == 1)? 1:0;
	}else{
		printk("bee_led_ctrl invalid gpio: %d\n",bee_led->led_gpio.gpio);
		gpio_free(bee_led->led_gpio.gpio);
	}
#endif

	gpio_request(bee_led->led_gpio.gpio, "bee_led_ctrl");
	gpio_direction_output(bee_led->led_gpio.gpio,bee_led->led_gpio.enable);
	
	init_timer(&LedTimer);
	LedTimer.expires = jiffies + jiffies_to_msecs(2);
	LedTimer.function = led_ctrl_func;
	LedTimer.data = 0;
	add_timer(&LedTimer);

	bee_led_class = class_create(THIS_MODULE, "bee_led_ctrl");
	ret = class_create_file(bee_led_class, (struct class_attribute *)&dev_attr_recovery);
	//if (ret)
	//{
	//        printk("Fail to class recovery/recovery_led.\n");
	//}

	return 0;
}

static int bee_led_gpio_remove(struct platform_device *pdev)
{
	del_timer(&LedTimer);
	gpio_free(bee_led->led_gpio.gpio);
	if(!bee_led){
		kfree(bee_led);
	}
	return 0;
}
static int bee_led_gpio_suspend(struct platform_device *pdev, pm_message_t state)
{
	del_timer(&LedTimer);
	if(bee_led->led_gpio.enable == 1){
		gpio_direction_output(bee_led->led_gpio.gpio,1);
	}
	return 0;
}
static int bee_led_gpio_resume(struct platform_device *pdev)
{
	gpio_direction_output(bee_led->led_gpio.gpio,0);
	bee_led->led_gpio.enable = 1;
	add_timer(&LedTimer);
	return 0;
}

#ifdef CONFIG_OF
static struct of_device_id gpio_dt_ids[] = {
	{ .compatible = "bee_led_ctrl" },
	{}
};
MODULE_DEVICE_TABLE(of, gpio_dt_ids);
#endif
static struct platform_driver bee_led_gpio_driver = {
	.driver = {
		.name    = "bee_led_ctrl",
		.owner   = THIS_MODULE,
#ifdef CONFIG_OF
		.of_match_table = of_match_ptr(gpio_dt_ids),
#endif
	},
	.suspend = bee_led_gpio_suspend,
	.resume = bee_led_gpio_resume,
	.probe		= bee_led_gpio_probe,
	.remove		= bee_led_gpio_remove,
};

static int __init bee_led_gpio_init(void)
{
	return platform_driver_register(&bee_led_gpio_driver);
}
subsys_initcall(bee_led_gpio_init);

static void __exit bee_led_gpio_exit(void)
{
	platform_driver_unregister(&bee_led_gpio_driver);
}
module_exit(bee_led_gpio_exit);

MODULE_AUTHOR("rpdzkj");
MODULE_DESCRIPTION("LED Drive for Bee");
MODULE_LICENSE("GPL v2");
MODULE_ALIAS("platform:rc5t583-gpio");
