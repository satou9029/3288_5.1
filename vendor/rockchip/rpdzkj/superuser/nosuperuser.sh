#!/system/bin/sh
if [ -f "/system/lib/libdummy.so" ]; then
    rm /system/lib/libdummy.so
fi
if [ -f "/system/xbin/daemonsu" ]; then
    rm /system/xbin/daemonsu
fi
if [ -f "/system/bin/install-recovery.sh" ]; then
    rm /system/bin/install-recovery.sh
fi
    cp /system/usr/superuser/root_disable/su /system/xbin/su