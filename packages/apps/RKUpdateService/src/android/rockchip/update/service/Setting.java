package android.rockchip.update.service;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;

public class Setting extends Activity {
	private static final String TAG = "RKUpdateService.Setting";
	private Context mContext;
	private Switch mSwh_AutoCheck;
	private Button mBtn_CheckNow;
	private SharedPreferences mAutoCheckSet;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.setting);
		mContext = this;
		mSwh_AutoCheck = (Switch)this.findViewById(R.id.swh_auto_check);
		mBtn_CheckNow = (Button)this.findViewById(R.id.btn_check_now);
		
		mAutoCheckSet = getSharedPreferences("auto_check", MODE_PRIVATE);
		
		mSwh_AutoCheck.setChecked(mAutoCheckSet.getBoolean("auto_check", true));
		mSwh_AutoCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				Editor e = mAutoCheckSet.edit();
				e.putBoolean("auto_check", isChecked);
				e.commit();
			}

		});
		
		mBtn_CheckNow.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent serviceIntent;
				serviceIntent = new Intent("android.rockchip.update.service");
                serviceIntent.putExtra("command", RKUpdateService.COMMAND_CHECK_REMOTE_UPDATING_BY_HAND);
                mContext.startService(serviceIntent);
			}
			
		});
	}
	
}
